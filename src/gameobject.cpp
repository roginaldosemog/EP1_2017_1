#include <iostream>
#include <string>
#include "gameobject.hpp"

using namespace std;

GameObject::GameObject(){}

GameObject::~GameObject(){}

void GameObject::setPosX(int value){
	this->posX = value;
}

void GameObject::setPosY(int value){
	this->posY = value;
}

void GameObject::setSprite(char sprite){
	this->sprite = sprite;
}

int GameObject::getPosX(){
	return this->posX;
}

int GameObject::getPosY(){
	return this->posY;
}

char GameObject::getSprite(){
	return this->sprite;
}