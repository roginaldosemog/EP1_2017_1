#include "player.hpp"
#include <iostream>
#include <stdio.h>
#include <ncurses.h>

using namespace std;

Player::Player(){}

Player::Player(char sprite, int posX, int posY){
	setSprite(sprite);
	setPosX(posX);
	setPosY(posY);
	setLastX(posX);
	setLastY(posY);
}

Player::~Player(){}

void Player::setAlive(bool alive){
	this->alive = alive;
}

void Player::setScore(int score){
	this->score = score;
}

void Player::setWinner(bool winner){
	this->winner = winner;
}

void Player::setLastX(int lastX){
	this->lastX = lastX;
}

void Player::setLastY(int lastY){
	this->lastY = lastY;
}

bool Player::getAlive(){
	return alive;
}

int Player::getScore(){
	return score;
}

bool Player::getWinner(){
	return winner;
}

int Player::getLastX(){
	return lastX;
}

int Player::getLastY(){
	return lastY;
}

void Player::movePlayer(){
	this->setLastX(this->getPosX());
	this->setLastY(this->getPosY());

	char direction = 'l';

	direction = getch();

	if(direction == 'w'){
		this->setPosY(this->getPosY()-1);
	} else if (direction == 's') {
		this->setPosY(this->getPosY()+1);
	} else if (direction == 'a') {
		this->setPosX(this->getPosX()-1);
	} else if (direction == 'd') {
		this->setPosX(this->getPosX()+1);
	}
}

void Player::returnMove(){
	this->setPosX(this->getLastX());
	this->setPosY(this->getLastY());
}