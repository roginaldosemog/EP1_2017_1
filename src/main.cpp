#include "map.hpp"
#include "gameobject.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"
#include "draw.hpp"
#include "collider.hpp"
#include <ncurses.h>
#include <iostream>

using namespace std;

int main()
{
	//Instanciar objetos
	Map * mapa = new Map(); //Instanciando mapa

	Player * player = new Player('@', 3, 3); //Instanciando player

	Draw * draw = new Draw(); //Instanciando classe Draw

	Collider * collider = new Collider(); //Instanciando classe colisor

	Trap * trap = new Trap(5, 5, 200);

	Bonus * bonus = new Bonus(8, 2, 300);

	//Ler mapa
	draw->initMap(mapa);
	
	draw->drawIntro();

	while(TRUE){
		//Procedimento ncurses - Iniciando tela
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();
		
		//printw("Current char: %c LastX: %d LastY: %d PosX: %d PosY:%d\n", mapa->returnMatriz(player->getPosX(), player->getPosY()), player->getLastX(), player->getLastY(), player->getPosX(), player->getPosY());
		draw->drawPlayer(mapa, player);
		draw->drawMap(mapa);
		player->movePlayer();
		collider->checkCollision(mapa, player);////CONSERTAR

		refresh();
		endwin();
	}

	delete(mapa);
	delete(player);
	delete(draw);
	delete(collider);
	
	return 0;
}