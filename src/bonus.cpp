#include "bonus.hpp"
#include <iostream>
#include <stdio.h>
#include <ncurses.h>

using namespace std;

Bonus::Bonus(){}

Bonus::Bonus(int posX, int posY, int score){
	setSprite('*');
	setPosX(posX);
	setPosY(posY);
	setScore(score);
}

Bonus::~Bonus(){}

void Bonus::setScore(int score){
	this->score = score;
}

int Bonus::getScore(){
	return score;
}