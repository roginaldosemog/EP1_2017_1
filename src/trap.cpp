#include "trap.hpp"
#include <iostream>
#include <stdio.h>
#include <ncurses.h>

using namespace std;

Trap::Trap(){}

Trap::Trap(int posX, int posY, int damage){
	setSprite('&');
	setPosX(posX);
	setPosY(posY);
	setDamage(damage);
}

Trap::~Trap(){}

void Trap::setDamage(int lastX){
	this->damage = damage;
}

int Trap::getDamage(){
	return damage;
}