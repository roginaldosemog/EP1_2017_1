#include "map.hpp"
#include <ncurses.h>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

Map::Map(){}

void Map::setRange(){

	ifstream map ("doc/map.txt");

	string aux;

	for(int i = 0; i < 37; i++){
		getline(map, aux);
		for(int j = 0; j < 89; j++){
			this->range[i][j] = aux[j];
		}
	}

	map.close();
}

void Map::getRange(){
	for(int i = 0; i < 37; i++){
		for(int j = 0; j < 89; j++){	
		 	printw("%c", this->range[i][j]);
		 	if(j >= 88){
		 		printw("\n");
		 	}
		}
	}
}

void Map::addObject(char sprite, int posx, int posy){
	this->range[posy][posx] = sprite; 
}

char Map::returnMatriz(int posX, int posY){
	char check[37][90];

	ifstream map ("doc/map.txt");

	string aux;

	for(int i = 0; i < 37; i++){
		getline(map, aux);
		for(int j = 0; j < 89; j++){
			check[i][j] = aux[j];
		}
	}

	map.close();

	return check[posX][posY];
}