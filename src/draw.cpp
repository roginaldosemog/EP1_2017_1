#include "map.hpp"
#include "player.hpp"
#include "draw.hpp"
#include <ncurses.h>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

Draw::Draw(){}

void Draw::drawIntro(){
	char range[37][89];

	ifstream intro ("doc/intro.txt");

	string aux;

	for(int i = 0; i < 37; i++){
		getline(intro, aux);
		for(int j = 0; j < 89; j++){
			range[i][j] = aux[j];
		}
	}

	intro.close();

	initscr();
	clear();
	keypad(stdscr, TRUE);
	noecho();

	for(int i = 0; i < 37; i++){
		for(int j = 0; j < 89; j++){	
		 	printw("%c", range[i][j]);
		 	if(j >= 88){
		 		printw("\n");
		 	}
		}
	}

	getch();

	refresh();
	endwin();
}

void Draw::initMap(Map * map){
	map->setRange();
}

void Draw::drawMap(Map * map){
	map->getRange();
}

void Draw::drawPlayer(Map * map, Player * player){
	map->addObject('-', player->getLastX(), player->getLastY());
	map->addObject(player->getSprite(), player->getPosX(), player->getPosY());
}