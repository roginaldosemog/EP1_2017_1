#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameobject.hpp"
#include <iostream>

class Bonus : public GameObject{
private:
	int score;

public:
	Bonus();
	Bonus(int posX, int posY, int score);
	~Bonus();

	void setScore(int score);
	int getScore();
};

#endif