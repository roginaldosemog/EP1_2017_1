#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <iostream>
#include <string>

class GameObject{
private:
	int posX;
	int posY;
	char sprite;

public:
//	virtual GameObject()=0;
	GameObject();
	virtual ~GameObject()=0;

	void setPosX(int posX);
	void setPosY(int posY);
	void setSprite(char sprite);
	int getPosX();
	int getPosY();
	char getSprite();
};

#endif