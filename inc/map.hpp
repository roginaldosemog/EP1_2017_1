#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <string>

class Map{
private:
	char range[37][90];

public:
	Map();

	void setRange();
	void getRange();
	void addObject(char sprite, int posx, int posy);
	char returnMatriz(int posX, int posY);
};

#endif