#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gameobject.hpp"
#include <iostream>

class Player : public GameObject{
private:
	bool alive;
	int score;
	bool winner;
	int lastX;
	int lastY;

public:
	Player();
	Player(char sprite, int posX, int posY);
	~Player();

	void setAlive(bool alive);
	void setScore(int score);
	void setWinner(bool winner);
	void setLastX(int lastX);
	void setLastY(int lastY);
	bool getAlive();
	int getScore();
	bool getWinner();
	int getLastX();
	int getLastY();

	void movePlayer();//movimenta o player
	void returnMove();
};

#endif