#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameobject.hpp"
#include <iostream>

class Trap : public GameObject{
private:
	int damage;

public:
	Trap();
	Trap(int posX, int posY, int damage);
	~Trap();

	void setDamage(int damage);
	int getDamage();
};

#endif