#ifndef DRAW_HPP
#define DRAW_HPP

#include <iostream>
#include <string>

class Draw{
private:
	//char introMatriz[38][90];
public:
	Draw();

	//void initIntro();
	void drawIntro();
	void initMap(Map * map);
	void drawMap(Map * map);
	void drawPlayer(Map * map, Player * player);
};

#endif