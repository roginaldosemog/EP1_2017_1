#ifndef COLLIDER_HPP
#define COLLIDER_HPP

#include <iostream>
#include <string>

class Collider{
private:
	
public:
	Collider();

	void checkCollision(Map * map, Player * player);	
};

#endif